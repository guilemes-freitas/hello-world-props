import { Component } from 'react';

class Developer extends Component{
    render(){
        return(
            <div>
                <h2>Dev: {this.props.name}</h2>
                <h4>Idade: {this.props.age}</h4>
                <h4>País: {this.props.country}</h4>
            </div>
        );
    }
}

export default Developer;